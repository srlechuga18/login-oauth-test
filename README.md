## Setting up the server url

Before setting up a provider, you need to specify the absolute url of your backend in server.js.

#strapi 
**config/server.js**

`module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: env('', 'http://localhost:1337'),
});`

## Warning
**If the user have required custom fields the creation of user will not work**

## Create Oauth App on provider site

**Info**
App name: Test App
Hompageurl: https://65e60559.ngrok.io _This url is an example_
App desc: App Description
Authorization callback URL: https://65e60559.ngrok.io/connect/github/callback _For strapi use {url}/connect/{provider}/callback see strapi docs for info_

## Strapi Config

**Fill the info**
Enable: ON
Client ID: 53de5258f8472c140917 _This is an example_
Client Secret: fb9d0fe1d345d9ac7f83d7a1e646b37c554dae8b _This is an example_
The redirect URL to your front-end app: unitydl://auth _Use the direction to your front app_

## Start Button
Create a button that links to **GET** **STRAPI_BACKEND_URL**/connect/{_provider_} (ex: https://strapi.mywebsite/connect/github).

`Application.OpenURL("http://0.0.0.0/connect/github");`

In unity you can use *OpenUrl* to start the process

_OAuthManager_ contains the process in this project

## Deep linking
This will create a shema to redirect from oauth to the app

See docs _https://docs.unity3d.com/Manual/enabling-deep-linking.html_

# Android

Place the following **AndroidManifest.xml** file into your Project’s _Assets/Plugins/Android_ folder. Unity automatically processes this file when you build your app:

`<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android" xmlns:tools="http://schemas.android.com/tools">
  <application>
    <activity android:name="com.unity3d.player.UnityPlayerActivity" android:theme="@style/UnityThemeSelector" >
      <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
      </intent-filter>
      <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:scheme="unitydl" android:host="auth" />
      </intent-filter>
    </activity>
  </application>
</manifest>
`
This line controls the scheme and path

`<data android:scheme="unitydl" android:host="auth" />`

## Login with new user

Send a web request to "http://0.0.0.0/auth/{provider}/callback?" + tokens

and manage the response it will incluse _username_ and _token_