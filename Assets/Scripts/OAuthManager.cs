using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine;
using System.Web;
using System;

public enum Provider { Yomobile, Github }
public class OAuthManager : MonoBehaviour
{
    [System.Serializable]
    public class StrapiUser
    {
        public string jwt;
        public User user;
    }
    [System.Serializable]
    public class User
    {
        public string username;
    }
    static OAuthManager _instance;
    public static OAuthManager Instance
    {
        get
        {
            if (!FindObjectOfType<OAuthManager>())
            {
                _instance = new GameObject("OAuthManager").AddComponent<OAuthManager>();
            }
            return _instance;
        }
    }
    private string deeplinkURL;
    public string ServerURL;
    [HideInInspector]
    public Provider Provider;
    private void Awake()
    {
        if (Instance == null)
        {
            Application.deepLinkActivated += onDeepLinkActivated;
            if (!string.IsNullOrEmpty(Application.absoluteURL))
            {
                onDeepLinkActivated(Application.absoluteURL);
            }
            else deeplinkURL = "[none]";
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        _instance = this;

        DontDestroyOnLoad(gameObject);
    }

    private void onDeepLinkActivated(string url)
    {

        deeplinkURL = url;

        string[] uriComponents = deeplinkURL.Split('?'); 

        string uri = uriComponents[0];

        string[] separators = {"://"};

        string[] protocolAndHost = uri.Split(separators, System.StringSplitOptions.RemoveEmptyEntries);

        if(protocolAndHost.Length > 1){
            // if(uriComponents.Length > 1){
            //     string vars = deeplinkURL.Split('?')[1];
                
            //     Debug.Log("tiene parametros " + vars);

                UIManager.Instance.SetErrorMessage(protocolAndHost[1]);
            // }
        }else{
            UIManager.Instance.SetErrorMessage("Empty");
        }
        

    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    UIManager.Instance.SetErrorMessage(webRequest.error);
                    UIManager.Instance.OpenFailedPanel();
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    UIManager.Instance.SetErrorMessage(webRequest.error);
                    UIManager.Instance.OpenFailedPanel();
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    string data = webRequest.downloadHandler.text;
                    StrapiUser stUsr = JsonUtility.FromJson<StrapiUser>(data);
                    UIManager.Instance.SetUsername(stUsr.user.username);
                    UIManager.Instance.OpenSuccessPanel();
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }

    public void Login()
    {
        if (Provider == Provider.Yomobile)
        {
            Application.OpenURL(ServerURL + "/connect/yomobile?callback=unitydl://scurry");
        }
        else if (Provider == Provider.Github)
        {
            Application.OpenURL(ServerURL + "/connect/github?callback=unitydl://scurry");
        }
    }
}
