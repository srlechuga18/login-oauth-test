using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    GameObject _loginPanel;
    GameObject _errorPanel;
    GameObject _loggedPanel;
    [SerializeField]
    TextMeshProUGUI _errorText;
    [SerializeField]
    TextMeshProUGUI _username;
    [SerializeField]
    TextMeshProUGUI _responseText;
    static UIManager _instance;
    public static UIManager Instance
    {
        get { return _instance; }
    }
    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
        }
        _instance = this;
        _loginPanel = transform.Find("LoginPanel").gameObject;
        _errorPanel = transform.Find("ErrorPanel").gameObject;
        _loggedPanel = transform.Find("LoggedPanel").gameObject;

    }

    private void Start()
    {
        BackToLogin();
    }

    public void Yogin()
    {
        OAuthManager.Instance.Provider = Provider.Yomobile;
        OAuthManager.Instance.Login();
    }
    public void GitLog()
    {
        OAuthManager.Instance.Provider = Provider.Github;
        OAuthManager.Instance.Login();
    }

    public void RequestDeepLinkScurry(string host)
    {
        Debug.Log("scurry://" + host);
        Application.OpenURL("scurry://" + host);
    }

    public void RequestDeepLinkYomobile()
    {
        string client_id = "WcxhOst3UFLrB6XsHFvZz9TgdvKHBrKS18n5zAXN";
        string callback_uri = "scurry://oauth";
        Application.OpenURL("yomobile://o/authorize/?response_type=code&amp;client_id=" + client_id + "&amp;redirect_uri=" + callback_uri);
    }

    public void BackToLogin()
    {
        _loginPanel.SetActive(true);
        _errorPanel.SetActive(false);
        _loggedPanel.SetActive(false);
    }

    public void Logout()
    {
        Debug.Log("login out");
        BackToLogin();
    }

    public void OpenSuccessPanel()
    {
        _loginPanel.SetActive(false);
        _errorPanel.SetActive(false);
        _loggedPanel.SetActive(true);
    }

    public void OpenFailedPanel()
    {
        _loginPanel.SetActive(false);
        _errorPanel.SetActive(true);
        _loggedPanel.SetActive(false);
    }

    public void SetErrorMessage(string text)
    {
        _errorText.text = text;
    }

    public void SetResponseMessage(string text){
        _responseText.text = text;
    }

    public void SetUsername(string user)
    {
        _username.text = user;
    }
}
